// When using implements, we are force to implement all the methods
// from Exception
class HttpException implements Exception {
  final String message;

  HttpException(this.message);

  @override
  String toString() {
    return message;
    // return super.toString();
  }
}
