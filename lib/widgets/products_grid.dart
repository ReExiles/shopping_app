import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './product_item.dart';
import '../providers/products_provider.dart';

class ProductsGrid extends StatelessWidget {
  final bool showOnlyFavorites;
  ProductsGrid(this.showOnlyFavorites);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final products = showOnlyFavorites ? productsData.favoriteItems : productsData.items;

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
      itemCount: products.length,
      padding: const EdgeInsets.all(10.0),
      // we can also use ChangeNotifierProvider.value and create: will replave value:
      // this can be used when we don't want to send anything to the listener
      // Use the ChangeNotifierProvider.value when using in List and grid when the builder recicles data

      // Good when we use a re-use and existing object, use the .value approach
      // Flutter cleans the value data when eplacing the current screen with another one (when using ChangeNotifierProvider / value).
      // Make sure to clean widgets in other cases.
      itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
        value: products[i],
        child: ProductItem(),
      ),
    );
  }
}
